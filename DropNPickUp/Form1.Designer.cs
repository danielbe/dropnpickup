﻿namespace DropNPickUp
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.ControlAdd = new System.Windows.Forms.Button();
            this.ControlOnTop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ControlAdd
            // 
            this.ControlAdd.Location = new System.Drawing.Point(12, 46);
            this.ControlAdd.Name = "ControlAdd";
            this.ControlAdd.Size = new System.Drawing.Size(23, 23);
            this.ControlAdd.TabIndex = 0;
            this.ControlAdd.Text = "+";
            this.ControlAdd.UseVisualStyleBackColor = true;
            this.ControlAdd.Click += new System.EventHandler(this.ControlAdd_Click);
            // 
            // ControlOnTop
            // 
            this.ControlOnTop.Location = new System.Drawing.Point(1, 1);
            this.ControlOnTop.Name = "ControlOnTop";
            this.ControlOnTop.Size = new System.Drawing.Size(85, 21);
            this.ControlOnTop.TabIndex = 1;
            this.ControlOnTop.Text = "always on top";
            this.ControlOnTop.UseVisualStyleBackColor = true;
            this.ControlOnTop.Click += new System.EventHandler(this.ControlOnTop_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 75);
            this.Controls.Add(this.ControlOnTop);
            this.Controls.Add(this.ControlAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.ShowInTaskbar = false;
            this.Text = "Drop N Pickup";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ControlAdd;
        private System.Windows.Forms.Button ControlOnTop;
    }
}

