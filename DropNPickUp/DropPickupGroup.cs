﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DropNPickUp
{
    class DropPickupGroup
    {
        private Form form;
        private TextBox box;
        private static int instanceCount;

        public DropPickupGroup(FormMain form)
        {
            this.form = form;
            Interlocked.Increment(ref instanceCount);

            Textbox();
            DropButton();
            PickupButton();
        }

        private void DropButton()
        {
            Action action = () => 
            {
                string s = Clipboard.GetText();
                box.Text = s;
            };
            ButtonModel("drop", 10, action);
        }

        private void PickupButton()
        {
            Action action = () =>
            {
                string text = box.Text;
                if (text.Equals(string.Empty))
                    Clipboard.Clear();
                else
                    Clipboard.SetText(text);
            };
            ButtonModel("pickup", 190, action);
        }

        private void Textbox()
        {
            //Interlocked.Increment(ref instanceCount);
            box = new TextBox();
            box.Location = new Point(90, 22 * instanceCount);
            //box.Name = "textBox1";
            box.Size = new Size(100, 20);
            //int numberOfTextBoxes = form.Controls.OfType<TextBox>().ToList().Count;

            form.Controls.Add(box);
        }

        private void ButtonModel(string text, int location, Action click)
        {
            Button button = new Button();
            button.Location = new Point(location, 22 * instanceCount);
            button.Size = new Size(80, 22);
            button.Text = text;

            button.Click += (sender, e) => { click(); };
            form.Controls.Add(button);
        }
    }
}
