﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DropNPickUp
{
    public partial class FormMain : Form
    {
        public List<Button> ButtonDropList;
        public List<Button> ButtonPickupList;
        public List<TextBox> TextFieldList;

        public FormMain()
        {
            InitializeComponent();
            AddItem();
        }

        private void ControlAdd_Click(object sender, EventArgs e)
        {
            AddItem();
            RepositionAddButton();
            ResizeForm();
        }

        private void AddItem()
        {
            DropPickupGroup dpg = new DropPickupGroup(this);
            //new TextField(this);
            //new ButtonPickup(this);
            //new ButtonDrop(this);
        }

        private void ControlOnTop_Click(object sender, EventArgs e)
        {
            TopMost = !TopMost;
        }

        private void RepositionAddButton()
        {
            int oldYLocation = ControlAdd.Location.Y;
            ControlAdd.Location = new System.Drawing.Point(10, oldYLocation + 22);
        }

        private void ResizeForm()
        {
            int oldHeight = ActiveForm.Size.Height;
            ActiveForm.Size = new Size(300, oldHeight + 22);
        }
    }
}
